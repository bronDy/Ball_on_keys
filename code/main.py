#Ball on keys
#python 3.6 with tkinter and notepad++
#https://www.bfxr.net/ and https://musiclab.chromeexperiments.com/Song-Maker/song for making music
def noPrint(*x, end='\n'):
	pass
print = noPrint


from random import randint

from keyboard.keyboard import *
from keyboard.key import *
from helpers.pixelColor import *

rules = "Can't place on button if there are most placed on that row, can't place adjacent"+ \
		"could add space press to temperary speed up the game and block space"


keyboard1 = None

def main():
	global keyboard1
	
	keyboardSeed = getRandomKeyboardSeed()
	
	keyboard1 = Keyboard(keyboardSeed)
	
	start()#init keyboard1
	
	#keyboard1.display()
	
borders = []
def start():
	global keyboard1
	global borders
	
	coordinatesOfBorders = keyboard1.addBorder('d', 3)#to update coordinatesOfBorders
	
	if coordinatesOfBorders[0] == -1:
		coordinatesOfBorders = keyboard1.addBorder('j', 3)#to update coordinatesOfBorders
		if coordinatesOfBorders[0] == -1:
			coordinatesOfBorders = keyboard1.addBorder('g', 3)#to update coordinatesOfBorders
			if coordinatesOfBorders[0] == -1:
				pass
			else:
				borders = coordinatesOfBorders#to update borders
		else:
			borders = coordinatesOfBorders#to update borders
	else:
		borders = coordinatesOfBorders#to update borders
	
	
def getRandomKeyboardSeed():
	
	seed = []
	
	for i in range(36):
		if randint(1,3) == 3:#33%
			if randint(1,4) == 4:#25%
				r1 = 2#2
			else:
				r1 = randint(2,3)#2 or 3
		else:
			r1 = 1#1
		
		seed.append(r1)
	
	#2's give one horizontal line, 3's one vertical at the bigging of the game
	minimumTwos = 15
	minimumThrees = 4
	
	if seed.count(2) < minimumTwos:
		#add twos to reach minimumTwos
		numberOfneededTwosToReachMinimum = minimumTwos - seed.count(2)
		for i in range(numberOfneededTwosToReachMinimum):
			seed[randint(0,len(seed)-1)] = 2#this might overwrite already existing two TODO FIXME
			
	if seed.count(3) < minimumThrees:
		#add twos to reach minimumThrees
		numberOfneededThreesToReachMinimum = minimumThrees - seed.count(3)
		for i in range(numberOfneededThreesToReachMinimum):
			seed[randint(0,len(seed)-1)] = 3#this might overwrite already existing three TODO FIXME
			
	#TODO what if there are too many 2's or 3's
	
	return seed
	
if __name__ == '__main__':
	main()

import tkinter#Python3
from tkinter import *#Python3
from PIL import ImageTk
from PIL import Image

tk = tkinter.Tk()
tk.title('Ball on keys')

can = Canvas(tk, width=1450, height=700)

tk.resizable(False, False)

can.configure(background='#51A3B2')#black

can.pack()

import winsound

failedTries = 0

def on_key_press(event):
	global keyboard1
	global arrowRotation
	global borders
	global failedTries
	
	if event.keysym == 'Shift_L': return
		
	print(event.char+' pressed')
	
	coordinatesOfBorders = keyboard1.addBorder(event.char.lower(), arrowRotation)
	
	if coordinatesOfBorders[0] == -1:
		print('failed to add border:' + coordinatesOfBorders[1])
		change_text(coordinatesOfBorders[1])
		failedTries = failedTries+1
		if failedTries >= 3:
			gameOver()
		change_text_append('REMAINING TRIES:' + str(3-failedTries))
		borders = keyboard1.coordinatesOfBorders
	else:
		borders = coordinatesOfBorders
		if keyboard1.allBordersActive():
			victory()
	
tk.bind("<KeyPress>", on_key_press)

#screen elements
ballImg = ''
ball1Filename = 'img/ball.png'
ball1Img = PhotoImage(file=ball1Filename)
ball2Filename = 'img/ball1.png'
ball2Img = PhotoImage(file=ball2Filename)
ball3Filename = 'img/ball2.png'
ball3Img = PhotoImage(file=ball3Filename)
ball4Filename = 'img/ball3.png'
ball4Img = PhotoImage(file=ball4Filename)
arrowFilename = 'img/arrow.png'
arrowImg = PhotoImage(file=arrowFilename)


from tkinter import font
smallFont = font.Font(size=19, underline=True)



x_coordinate = 280
y_coordinate = 420
ball = can.create_image(x_coordinate, y_coordinate, anchor=NW, image=ballImg)
arrrow = can.create_image(1310, 550, anchor=NW, image=arrowImg)

fps = 160
T = int(1000/fps)
T=4

ball_w = 38
ball_h = 38

x_dir = 1#right
y_dir = 1#down

arrowRotation = 1

def changeArrowOrientation():
	global arrowRotation
	arrowRotation = arrowRotation + 1
	if arrowRotation >= 5: arrowRotation = 1
	changeBallImage()
	#@winsound.PlaySound('sound/hit3.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	randFrom3 = randint(1,3)
	if randFrom3 == 1: winsound.PlaySound('sound/hit1.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	elif randFrom3 == 2: winsound.PlaySound('sound/hit2.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	else: winsound.PlaySound('sound/hit3.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	
def changeBallImage():
	global ballImg
	if ballImg == ball1Img: ballImg = ball2Img
	elif ballImg == ball2Img: ballImg = ball3Img
	elif ballImg == ball3Img: ballImg = ball4Img
	else: ballImg = ball1Img
	
txt = 'Activate all borders to win. Press key to activate it\'s border.\nDon\'t activate border multiple times. Don\'t let the time run out'
def change_text(txt1):
	global txt
	txt = txt1
def change_text_append(txt1):
	global txt
	txt = txt + '\n' + txt1



extraImg = None#to be filled by gameOver or victory functions

extraToDraw = False
extraImgGameOverLoc = 'img/gameOver.png'
extraImgWinLoc = 'img/youwin.png'

gameNotOver = True

def victory():
	global extraToDraw
	global extraImg
	global gameNotOver
	print('victory victory victory victory')
	extraImg = PhotoImage(file=extraImgWinLoc)
	extraToDraw = True
	gameNotOver = False
	winsound.PlaySound('sound/song.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	
def gameOver():
	#return
	global extraToDraw
	global extraImg
	global gameNotOver
	print('game over :<(')
	extraImg = PhotoImage(file=extraImgGameOverLoc)
	extraToDraw = True
	gameNotOver = False
	if randint(1,2) == 2:
		winsound.PlaySound('sound/gameOver2.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	else: winsound.PlaySound('sound/gameOver.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
	
def FRAME():
	global x_coordinate
	global y_coordinate
	global x_dir#1 or -1
	global y_dir#1 or -1
	global arrowImg
	global arrowRotation
	global txt
	
	x_coordinate = x_coordinate + x_dir
	y_coordinate = y_coordinate + y_dir
	
	#cls
	can.delete("all")
	
	#show ball in next frame position
	can.create_image(x_coordinate, y_coordinate, anchor=NW, image=ballImg)
	
	#show keyboard outline
	can.create_line(60, 60, 1350, 60, fill="darkblue", width=10)#upper
	can.create_line(60, 60, 60, 180, fill="darkblue", width=10)#left
	can.create_line(60, 180, 95, 180, fill="darkblue", width=10)#left
	can.create_line(95, 180, 95, 310, fill="darkblue", width=10)#left
	can.create_line(95, 310, 130, 310, fill="darkblue", width=10)#left
	can.create_line(130, 310, 130, 440, fill="darkblue", width=10)#left
	can.create_line(130, 440, 165, 440, fill="darkblue", width=10)#left
	can.create_line(165, 440, 165, 570, fill="darkblue", width=10)#left
	can.create_line(165, 570, 1065, 570, fill="darkblue", width=10)#bottom
	can.create_line(1350, 60, 1350, 190, fill="darkblue", width=10)#right
	can.create_line(1350, 190, 1385, 190, fill="darkblue", width=10)#right
	can.create_line(1385, 190, 1385, 320, fill="darkblue", width=10)#right
	can.create_line(1385, 320, 1290, 320, fill="darkblue", width=10)#right
	can.create_line(1290, 320, 1290, 450, fill="darkblue", width=10)#right
	can.create_line(1290, 450, 1065, 450, fill="darkblue", width=10)#right
	can.create_line(1065, 450, 1065, 570, fill="darkblue", width=10)#right
	
	#show borders
	for border in borders:
		can.create_rectangle(border[0], border[1], border[2], border[3], fill="blue")
		
	#text
	text_id = can.create_text(600,630,fill="yellow", font="Times 25 italic bold", text='')
	can.itemconfig(text_id, text=txt)
	
	#flip direction
	if x_dir == 1:
		if y_dir == 1:
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w/2, y_coordinate+ball_h):
				y_dir = -1
				changeArrowOrientation()
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w, y_coordinate+ball_h/2):
				x_dir = -1
				changeArrowOrientation()
		else:
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w/2, y_coordinate):
				y_dir = 1
				changeArrowOrientation()
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w, y_coordinate+ball_h/2):
				x_dir = -1
				changeArrowOrientation()
	else:
		if y_dir == 1:
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w/2, y_coordinate+ball_h):
				y_dir = -1
				changeArrowOrientation()
			if ImageUtils.get_pixel_color(can, x_coordinate, y_coordinate+ball_h/2):
				x_dir = 1
				changeArrowOrientation()
		else:
			if ImageUtils.get_pixel_color(can, x_coordinate+ball_w/2, y_coordinate):
				y_dir = 1
				changeArrowOrientation()
			if ImageUtils.get_pixel_color(can, x_coordinate, y_coordinate+ball_h/2):
				x_dir = 1
				changeArrowOrientation()
	
	#draw arrow
	angle = -90
	image = Image.open('img/arrow.png')
	arrowImg = ImageTk.PhotoImage(image.rotate(angle*arrowRotation))
	arrrow = can.create_image(1310, 550, anchor=NW, image=arrowImg)
	
	if extraToDraw == True:
		can.create_image(220, 65, anchor=NW, image=extraImg)
		return
	
	tk.after(T, FRAME)#T = one frame every second divided by 60

FRAME()

countdown_sec = 500#TODO change difficulty or change level

countdown_stringVar = StringVar()

def countdown():
	global countdown_sec
	global countdown_stringVar
	global gameNotOver

	print('countdown:'+str(countdown_sec))
	if countdown_sec <= 0:
		gameOver()
	else:
		countdown_sec = countdown_sec - 1
		left = keyboard1.numBordersLeft()
		countdown_stringVar.set(str(countdown_sec) +' seconds remaining, ' + left + ' keys without borders.')
		if left == -1: victory()
		if gameNotOver:
			tk.after(1000, countdown)

countdown()

countdown_stringVar_label = Label(tk, font=smallFont, textvariable=countdown_stringVar).pack()

tk.mainloop()