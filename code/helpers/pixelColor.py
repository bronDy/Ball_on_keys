
class ImageUtils:

	@staticmethod
	def get_pixels_of(canvas):
		width = int(canvas["width"])
		height = int(canvas["height"])
		colors = []

		for x in range(width):
			column = []
			for y in range(height):
				column.append(ImageUtils.get_pixel_color(canvas, x, y))
			colors.append(column)

		return colors

	@staticmethod
	def get_pixel_color(canvas, x, y):
		ids = canvas.find_overlapping(x, y, x, y)

		if len(ids) > 0:
			return 1#'not white'

		return 0#"WHITE"