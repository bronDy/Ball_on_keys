from random import randint
def noPrint(*x, end='\n'):
	pass
print = noPrint

class Key():
	
	char = ''
	adjacent = []# ex. for key a => [ { "U":"q"}, {"U":"w"}, {"D":"z"}, {"R":"s" } ]
	
	def __init__(self, char, adjacent):
		self.char = char
		self.adjacent = adjacent
	
	#return -1 if no key found in given direction else return one key from given direction
	def getRandomAdjacent(self, direction):#direction = 'U','D','L' or 'R'
		
		found = 0
		foundKeys = []
		
		for elem in self.adjacent:
			
			try:
				
				if elem[direction]:
					
					foundKeys.append(elem[direction])
					found = found+1#one is at elem[direction]
					
			except KeyError as e: pass
		
		if found == 0: return -1
		
		elif found >= 1:#shouldn't be more than 2
			return foundKeys[randint(0, found-1)]
			
		else: return foundKeys[0]
		
	def removeConnectionTo(self, char):
		
		foundAt = -1
		print('\nFor char:',end='')
		print(self.char)
		print('adj',end='')
		print(self.adjacent)
		print('removing:',end='')
		print(char)
		
		for i, elem in enumerate(self.adjacent):
				
			try:
				if elem['U'] == char: foundAt = i
			except KeyError as e: pass
			try:
				if elem['R'] == char: foundAt = i
			except KeyError as e: pass
			try:
				if elem['D'] == char: foundAt = i
			except KeyError as e: pass
			try:
				if elem['L'] == char: foundAt = i
			except KeyError as e: pass
			
		if foundAt != -1:
			print('index to remove at:',end='')
			print(foundAt)
			del self.adjacent[foundAt]
		else:
			return 'can\'t remove connection from ' + self.char + ' to ' + char + ' because they are not connected.'
			
		print('For char:',end='')
		print(self.char)
		print('adj',end='')
		print(self.adjacent)
			
		return
		
	def hasAdjacent(self):
		return 0 if len(self.adjacent) == 0 else 1
	
	def __str__(self):
		return str(self.adjacent)