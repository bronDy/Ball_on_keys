from keyboard.key import *
def noPrint(*x, end='\n'):
	pass
print = noPrint

class Keyboard():
	
	keysChars = '1234567890'+'qwertyuiop'+'asdfghjkl'+'zxcvbnm'
	
	#list of keys which all have connections to other keys
	keys = []
	
	'''
	key coordinates are similar to this
	keySize = 50
	1 = 50, 50				2 = 50+10, 50
	a = 50+10, 100+10		s = 50+20, 100+10
	'''
	coordinatesOfBorders = []
	
	#allCoordinatesOfBorders actually holds keyboard key locations in form (x,1 ,y1, x2, y2)
	allCoordinatesOfBorders = []#search by index of keyChars
	#x1, y1 and x2, y1 are starts of vertical lines
	#x1, y1 and x1, y2 are starts of horizontal lines
	
	
	def __init__(self, seed):
		
		#link keys with adjacent keys
		for i, c in enumerate(self.keysChars):
			
			adj = []
			
			even10KeysChars = list(self.keysChars)
			even10KeysChars.insert(29, '.')#. = temp for third row which has 9 keys
			
			#add bottom chars
			#fix 1,q,p,a because their bottom ones are not correct
			if c not in ['1','q','a']:
				try:
					adj.append( { 'D' : even10KeysChars[i+9] } )
				except IndexError as e: pass
			
			#fix 1,q,p,a because their bottom ones are not correct
			if c != 'p':	
				try:
					adj.append( { 'D' : even10KeysChars[i+10] } )
				except IndexError as e: pass
				
			
			#add up chars
			itt=0
			
			if i >= 10:
				if i >= 29: itt = 1#fix last row
				try:
					adj.append( { 'U' : even10KeysChars[i-10+itt] } )
				except IndexError as e: pass
				if c not in ['p']:
					try:
						adj.append( { 'U' : even10KeysChars[i-9+itt] } )
					except IndexError as e: pass
						
			#left and right adjacent chars
			if c not in ['1','q','a','z']:
				try:
					adj.append( { 'L' : self.keysChars[i-1] } )
				except IndexError as e: pass
			if c not in ['0','p','l','m']:
				try:
					adj.append( { 'R' : self.keysChars[i+1] } )
				except IndexError as e: pass
			
			
			self.keys.append( Key(c, adj) )
			
			
		#temp
		print('seed:')
		print(seed[:10])
		print(seed[10:20])
		print(seed[20:29])
		print(seed[29:])
		
		
		#populate allCoordinatesOfBorders
		keySize = 130
		paddingX = -20
		paddingY = -20
		
		row = 0
		nextRow = 0
		shiftedIntoX = 0
		SHIFT_FOR_shiftedIntoX = 35
		
		for i in range(1, len(self.keysChars)+1):
			
			x0 = keySize*i + nextRow + shiftedIntoX - keySize/2
			y1 = keySize + row - keySize/2
			x2 = keySize*i+keySize + paddingX + nextRow + shiftedIntoX - keySize/2
			y2 = keySize+keySize + row + paddingY - keySize/2
			
			
			self.allCoordinatesOfBorders.append([x0, y1, x2, y2])
			
			#x1, y1 and x2, y1 are vertical lines
			
			#x1, y1 and x1, y2 are horizontal lines
			
			if i == 10:
				y1 = y1 + keySize
				row = row + keySize
				nextRow = nextRow + (-keySize *10)
				shiftedIntoX = shiftedIntoX + SHIFT_FOR_shiftedIntoX
				
			if i == 20:
				y1 = y1 + keySize
				row = row + keySize
				nextRow = nextRow + (-keySize *10)
				shiftedIntoX = shiftedIntoX + SHIFT_FOR_shiftedIntoX
				
			if i == 29:
				y1 = y1 + keySize
				row = row + keySize
				nextRow = nextRow + (-keySize *9)
				shiftedIntoX = shiftedIntoX + SHIFT_FOR_shiftedIntoX
		
		#apply seed to keys
		for seedVal, keyChar in zip(seed, self.keysChars):
			if seedVal == 2:#vertical
				self.addBorder(keyChar, 'U')
			elif seedVal == 3:#horizontal
				self.addBorder(keyChar, 'R')
			else: pass#number 1

		
	#add to border by removing adjacent Key, and this Key from that key so same border can not be created twice
	#returns self.coordinatesOfBorders or if trying to add illegal border [-1, 'msg why adding on that side of char would be illegal']
	def addBorder(self, char, side):
		
		if char not in self.keysChars: return [-1, '"' + str(char) + '" is not a legal key'] 
		
		if side==1: side='D'
		if side==2: side='L'
		if side==3: side='U'
		if side==4: side='R'
		
		print('add border to ' + char + ' at side:' + str(side))
		
		#disconnect this key with value of char to side key
		randAdjChar = ''
		randAdjChar2 = ''
		
		for key in self.keys:
			if key.char == char:
				if key.hasAdjacent():
					randAdjChar = key.getRandomAdjacent(side)
					if randAdjChar == -1:
						print('Trying to add border to key \'' + char + '\' on side ' + side + ' which has no adjacent key without border')
						return [-1,'Trying to add border to key \'' + char + '\' on side ' + side + ' which has no adjacent key without border']
					key.removeConnectionTo(randAdjChar)
					
					if side not in ['L', 'R']:
						randAdjChar2 = key.getRandomAdjacent(side)
						if randAdjChar2 == -1: pass
						else: key.removeConnectionTo(randAdjChar2)
					
				else:
					print('Trying to add border to key \'' + char + '\' which has no place to add border')
					return [-1,'Trying to add border to key \'' + char + '\' which has no place to add border']
				
		#now remove from other key this key
		for key in self.keys:
			if key.char == randAdjChar:
				key.removeConnectionTo(char)
			
		if randAdjChar2 == -1: pass
		else:			
			if side not in ['L', 'R']:
				for key in self.keys:
					if key.char == randAdjChar2:
						key.removeConnectionTo(char)
		
		
		#add new border to screen
		
		keySize = 130
		thickness = 10
		offset = 10
		
		border = self.allCoordinatesOfBorders[self.keysChars.index(char)]#[x1, y1, x2, y2]
		
		#x1, y1 and x2, y1 are vertical lines
		
		#x1, y1 and x1, y2 are horizontal lines
		
		if side=='U': border = [border[0]-offset, border[1]-thickness, border[0]+keySize-offset, border[1]]
		elif side=='D': border = [border[0]-offset, border[3], border[0]+keySize-offset, border[3]+thickness]
		elif side=='L': border = [border[0]-thickness, border[1]-offset, border[0], border[1]+keySize-offset]
		else: border = [border[2], border[1]-offset, border[2]+thickness, border[1]+keySize-offset]#'R'
		
		self.coordinatesOfBorders.append(border)
		
		
		return self.coordinatesOfBorders
		
	def display(self):
		itt = 0
		for key in self.keys:
			print(key.char+str(key.adjacent), end='')
			if key.char in ['0','p','l','m']:
				itt = itt + 1
				print('\n'+' '*itt, end='')
	
	def allBordersActive(self):
		for key in self.keys:
			if key.hasAdjacent() >= 1:
				print('key:'+key.char, end='')
				print(' still has adj:' + str(key.adjacent))
				self.display()
				return False
		
		return True
				
	def numBordersLeft(self):
		bordersLeft = []
		for key in self.keys:
			if key.hasAdjacent() >= 1:
				bordersLeft.append(key.char + '=>' + str(key.adjacent))
				
		print(len(bordersLeft))
		if len(bordersLeft) > 3: return str(len(bordersLeft))
			
		if len(bordersLeft) > 0: return str(len(bordersLeft)) + ':' + str(bordersLeft)
			
		if len(bordersLeft) == 0: return -1